<?php

class Base_EventObserver_Model_Observer
{
    public function applyCoupon($observer) {
    	//Mage::app()->getCookie()->set('date2', date("l jS \of F Y h:i:s A"));
        
        $response = $observer->getResponse();
        $request  = $observer->getRequest();
        $couponCode = (string)$request->getParam('coupon_code');
        if (!$couponCode or !strlen($couponCode)) {
            return;
        }
        $session    = Mage::getSingleton('checkout/session');
        $cart       = Mage::getSingleton('checkout/cart')->getQuote();
        $cart->getShippingAddress()->setCollectShippingRates(true);
        $cart->setCouponCode(strlen($couponCode) ? $couponCode : '')
            ->collectTotals()
            ->save();
    }
}