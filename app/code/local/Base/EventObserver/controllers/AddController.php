<?php

class Base_EventObserver_AddController extends Mage_Core_Controller_Front_Action
{
	public function indexAction(){
		$product = $this->getRequest()->getParam('product');
		$coupon_code = $this->getRequest()->getParam('coupon_code');
		$key = Mage::getSingleton('core/session')->getFormKey();
		if (empty($product)){
			$this->_redirect('');
		} else {
			$this->_redirect('checkout/cart/add', array('product'=>$product, 'form_key'=>$key, 'coupon_code'=>$coupon_code));
		}
	}
}