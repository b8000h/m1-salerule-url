1.直接跳到购物车页面，（不需要到产品详情页）
2.
在这其间：11.24号 20:00:00 - 22:59:59 PM ( UTC TIME)
HC29、HC290、HC291 ----在这3小时内到达购物车再减10USD
G36、G3601、G3602----在这3小时内到达购物车再减10USD



1. Product 加入 Cart
2. 进入 Cart
3. 应用 Salerule


1. 设置 coupon code，重点是所适用的 products

http://www.magecorner.com/

You want to offer a product with a coupon discount but would like this done in one step. 
What you do not want is the customer having to click on a link to add the product to the cart and then have to enter the coupon code. 
This could be used in an email or some other promotion.

We can go about adding products to cart with coupons. 
One of the simplest and most effective ways is to use Magento’s observer and event handler. 
In this case we observer the checkout_cart_add_product_complete event and check for a coupon code. 
The end result is a url ending with /checkout/cart/add/product/337?coupon_code=abc123

This will take the coupon code and validate/apply it to the product in the cart before displaying the Magento cart page.

Just a note you will need a valid coupon for the product or cart.



---

Auto Apply Discount on Product Page based on URL
I am wondering if anyone can offer advice on the best/easiest way to accomplish the following:

I would like to be able to change the price of a product based on a URL parameter. 
So for example if the regular price of a product is $100, I would like to be able to specify some sort of URL parameter that would allow me to have the price of the product be a lower amount.

I am currently considering creating coupon codes and auto applying them using the linksture extension and then having some logic lookup what the discount is and then apply it on the product page. 
I think this will accomplish the goal, but wondering if there is a better/simpler/cleaner way.

---

			<!--
			<catalog_product_get_final_price>
		        <observers>
		            <base_eventobserver_getfinalprice>
		                <type>singleton</type>
		                <class>base_eventobserver/observer</class>
		                <method>getFinalPrice</method>
		            </base_eventobserver_getfinalprice>
		        </observers>
		    </catalog_product_get_final_price>
			-->

	/**
	 * Update price for product items
	 *
	 * @param Varien_Event_Observer $observer
	 */
	public function getFinalPrice($observer) {

	    /* @var Mage_Catalog_Model_Product $product */
	    $product = $observer->getEvent()->getProduct();

	    if ($buyRequest = $product->getCustomOption('info_buyRequest')) {

	        $buyRequest = unserialize($buyRequest->getValue());

	        if (is_array($buyRequest) && isset($buyRequest['custom_discount'])) {

	            $product->setFinalPrice(YOUR_LOGIC_BASED_ON_PARAMETER($buyRequest['custom_discount']));
	        }
	    }
	}


On the product page itself (in the product view template), you can use $_product->setFinalPrice(YOUR_LOGIC_BASED_ON_PARAMETER($this->getRequest()->getParam('YOUR_PARAMETER')); to change the displayed price as well.

YOUR_LOGIC_BASED_ON_PARAMETER() could be a helper method in your custom module.

Note that this changes the product price dynamically, so that it will show up with this price as normal price in cart and order. No explicit discount will be shown.
